---
layout: markdown_page
title: "UX Research"
---

### On this page

{:.no_toc}

- TOC
{:toc}

## UX Research

The goal of UX Research at GitLab is to connect with GitLab users all around the world and gather insight into their behaviors, motivations, and goals when using GitLab. We use these insights to inform and strengthen product and design decisions.

If you're a UX Researcher who is new to GitLab, check out [this page][ux-researcher] for information that's specific to your role.

### Training resources
UX Researchers aren't the only GitLabbers who conduct user research. Other roles, like Product Managers and Product Designers, frequently conduct research, too, with guidance from the UX Research team. If you need to conduct a user research study, here are some resources to help get you started:

* [Tips for facilitating user interviews](https://www.youtube.com/watch?v=-6U5p6A4WWE)
* [How to write a strong hypothesis](https://www.youtube.com/watch?v=i-LIu-zDOOM)
* More training in process (Coming soon!)

### GitLab First Look

GitLab First Look (formerly the UX Research Panel) is a group of users who have opted in to receive research studies from GitLab. The UX Research team manages and maintains this program. To find out more or to join, please visit [GitLab First Look](/community/gitlab-first-look/index.html).

### UXR_Insights Repository
The [UXR_Insights repository](https://gitlab.com/gitlab-org/uxr_insights) is the single source of truth (SSOT) for all user insights discovered by GitLab’s UX Researchers and Product Designers. A directory of completed research is available in the repository's [ReadMe](https://gitlab.com/gitlab-org/uxr_insights/blob/master/README.md) file.

### How to request research

1. For usability testing, user interviews, card sorts, surveys, or if you are not sure what form of research needs to take place, create a new issue using the `Research proposal` [template](https://gitlab.com/gitlab-org/ux-research/blob/master/.gitlab/issue_templates/Research%20proposal.md) in the [UX research project](https://gitlab.com/gitlab-org/ux-research).    

    For beta testing, create a new issue using the `Beta testing proposal` [template](https://gitlab.com/gitlab-org/ux-research/blob/master/.gitlab/issue_templates/Beta%20testing%20proposal.md) in the [UX research project](https://gitlab.com/gitlab-org/ux-research).

1. `@` mention the relevant UX Researcher, Product Designer, UX Manager, and Product Manager for the [product stage](/handbook/product/categories/#devops-stages). Ensure you answer all questions outlined in the template.

    * You can find out who the relevant UX Researcher and/or Product Designer is by looking at the [team page](/company/team/) and filtering by the `UX` department.

    * Anybody across GitLab can raise a proposal. This includes UX Researchers.

1. The UX Researcher will review the issue and may respond with some follow-up questions.

    * We want to lessen the time that researchers spend within issues and/or Slack soliciting research requirements. As a rule of thumb, if we have gone back and forth more than 3 times, it's time for a video call.

    * Similarly, some features are more complex than others. A UX Researcher needs to fully understand the feature they are testing and why they are testing it. Sometimes, it's much easier to get a grasp on the feature's history, your existing plans and plans for the future when you talk with us. In cases such as these, the UX Researcher will schedule a kick-off call with all relevant stakeholders (including the Product Designer, UX Manager, and Product Manager) to run through the research proposal.

1. In collaboration with the Product Manager and UX Manager, the UX Researcher will determine the priority of the study and schedule it accordingly.

1. The UX Researcher will book a wash-up meeting with all relevant stakeholders when the results of the study are available.

### Beta Testing at GitLab

#### Aims
* Collect quotes from users that we can use in release posts.
* Generate feature awareness.
* Identify bugs and/or improvements for a feature.
* Gather general user feedback.


#### FAQs about Beta Testing

1. How can I request beta testing?

    You can request beta testing by following the instructions outlined in ['How to request research'](/handbook/engineering/ux/ux-research/#how-to-request-research).


1.  How do users see a feature in beta?

    First iteration: Test feature(s) as they are added to a [release candidate](https://gitlab.com/gitlab-org/release/docs/blob/master/general/release-candidates.md).

    Future iterations: Route beta users to a Canary version of GitLab that runs pre-release code. Use feature flags.


1. How do users provide feedback about a feature?

    By completing a survey.


1. What if I don't know what release candidate the feature will be added in?

    That's okay. All survey questions should be finalized in advance of the first release candidate, so that the UX Researcher can build the survey and the accompanying mail campaign ahead of time.

    When the feature is added to a release candidate, ping the relevant UX Researcher in the issue you have created. The UX Researcher will then distribute the survey to users.


1. What if the feature is added to a late release candidate? Is there any point in still beta testing the feature?

    Yes, you can still beta test the feature.

    The overall aims of your study will be to:
    * Generate feature awareness.
    * Identify bugs and/or improvements for a feature.
    * Gather general user feedback.

    It may be beneficial to delay sending the survey until the official release on the 22nd to save users the effort of downloading a release candidate.


1. Can I request beta testing for a feature that isn't labeled as a [beta](/handbook/product/#alpha-beta-ga) release?

    Yes, you can test any feature in an upcoming release candidate/milestone.


1. How long will a feature be tested for?

    This depends on which release candidate the feature is added to and what the ultimate aims of the study are.

    If the feature is added to an early release candidate. Your main aim might be to:

    * Collect quotes from users that we can use in release posts.

    In which case, there's not much point running the survey past the 22nd of the month.

    If the feature is added to a later release candidate. Your main aims might be to:

    * Generate feature awareness.
    * Identify bugs and/or improvements for a feature.
    * Gather general user feedback.

    Therefore, we recommend running the survey until it stops receiving responses.

1. What are some example questions that I could ask users?

    * How could the feature be improved? (Open text)
    * What, if anything, didn’t work as you expected it to? (Open text)
    * Does this feature help you accomplish X? (Rating scale)
        * Please explain your answer (Open text)
    * What triggers would prompt you to use this feature? (Open text)
    * How likely is it that you would use this feature? (Rating scale)
        * Why is it unlikely that would use this feature? (Open text)
    * What do you most like about the feature? (Open text)
    * Overall, how easy was it to use the feature? (Rating scale)

### UX Research label
Both the [GitLab CE project](https://gitlab.com/gitlab-org/gitlab-ce) and [GitLab EE project](https://gitlab.com/gitlab-org/gitlab-ee) contain a `UX Research` label. The purpose of this label is to help Product Designers and Product Managers keep track of issues which they feel may need UX Research support in the future or which are currently undergoing UX Research. 

UX Researchers are not responsible for maintaining the `UX Research` label.

The `UX Research` label should not be used to request research from UX Researchers. Instead, please follow the process outlined in [How to request research](/handbook/engineering/ux/ux-research/#how-to-request-research).

Learn more about how we use Workflow labels in the [GitLab Docs](https://docs.gitlab.com/ee/development/contributing/issue_workflow.html).

[ux-researcher]: https://about.gitlab.com/handbook/engineering/ux/ux-researcher/
[ux-guide]: https://docs.gitlab.com/ee/development/ux_guide/
[ux-label]: https://gitlab.com/groups/gitlab-org/issues?scope=all&state=opened&utf8=%E2%9C%93&label_name%5B%5D=UX
[ux-ready-label]: https://gitlab.com/groups/gitlab-org/issues?scope=all&state=opened&utf8=%E2%9C%93&label_name%5B%5D=UX+ready
[gitlab-design-project-readme]: https://gitlab.com/gitlab-org/gitlab-design/blob/master/README.md
[twitter-sheet]: https://docs.google.com/spreadsheets/d/1GDAUNujD1-eRYxAj4FIYbCyy8ltCwwIWqVTd9-gf4wA/edit
