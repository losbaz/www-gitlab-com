---
layout: markdown_page
title: "Growth and Enablement UX"
---
<!--note about this page: I realize our stage groups are quite different and if we include everything on this page it can get very long and rambling. Let's think about our audience and how to best structure the information and split off if need be. -->


## Overview
The Growth and Enablement areas are made up of seven groups: Growth (Growth, Fulfillment, Telemetry) and Enablement (Memory, Distribution, Ecosystem and Geo). While these are quite different areas with their own Jobs to Be Done and product strategies, the over-arching theme is making GitLab easier to install, use and and integrate with, so that people can get started fast and without friction, and use GitLab where and when works for them. GitLab believes that **everyone can contribute**, and this is central to our strategy.




<!--
To complete this section, we might answer questions like:
- who are we designing for and what is their desired experience?
- how do we align the desired experience to company strategy
- what is our high level approach and philosophy?
These are hard questions! Not meant to be answered in a day, we can do iterative user research to uncover this information over time. -->

### Our customer
Coming soon! We will answer questions like:
- who are the customers we're selling to?
- what are they like at individual or organizational level?
- what value do we provide them?
- what do they think of our product?
- what improvements do they want?
- how do they interact with the product?
- what are the personas we've created?
- what are their purchase concerns? 
- What is their purchase/use journey?


### Our user
Coming soon! We will answer questions like:
- who are the users we're designing for?
- what are they like at individual or organizational level?
- what value do we provide them?
- how can we best communicate that value?
- what do they think of our product?
- what improvements do they want?
- how can we help them engage more with our product?
- how can we help them learn our product?
- how do they interact with the product?
- what are the personas we've created?
- What is their purchase/use journey?

## Customer Journey
As a team we feel that it's really important to understand, document and consider the entire customer journey during design. To that end we will document what we know as we learn it and link that documentation here.

### Customer Journey/Process for Fulfillment/Customer Transactions

When working through transactional issues related to sign-up, trials and upgrades it helps to break down the task into pieces. This way of working through issues enables product designers to document the beginning and end of a user journey in an easily digestible way for everyone. It's based very loosely on a talk from Jared Spool regarding "Content and Design".
* Entry Point(s): The initial touch points of user interactions. (i.e. A Page, CTA or Form etc)
* Decision: Giving users the ability to decide on Products, Options or Packages.
* Confirmation: Summary of a successful or unsuccessful purchase.

These steps won't always be needed and won't always be linear. For instance, an Entry Point may also be a point at which a user selects a Product. 



## Our Jobs to Be Done
Coming soon! We will list the main JTBD, along with links to helpful material such as an XP baseline, usability score, walkthrough video, journey map, recommendations for improvement, etc. Placeholders are good if we know the JTBD but haven't worked on it.

#### JTBD for Fulfillment
##### Start a GitLab trial
* Job Description: When (situation), I want to (motivation), so I can (expected outcome).
* Baseline Epic (with walkthrough and video): [1332](https://gitlab.com/groups/gitlab-org/-/epics/1332)
* Baseline Issue: [1355](https://gitlab.com/groups/gitlab-org/-/epics/1355)
* Baseline Score: D- (Q2) 
* Recommendations: [1356](https://gitlab.com/groups/gitlab-org/-/epics/1356)

##### Purchase a GitLab Plan
TBD
##### Upgrade a GitLab Plan
TBD
##### Renew a GitLab Plan
TBD
##### Buy an Add-on
TBD
##### Cancel a GitLab Plan
TBD
##### Downgrade a GitLab Plan
TBD


#### JTBD for Geo
TBD

#### JTBD for Distribution
TBD

#### JTBD for Ecosystem
TBD

#### JTBD for Growth
TBD 


## Our team
<!--Let's talk about who we are and link to our ReadMe's-->

* [Jacki Bauer](https://about.gitlab.com/company/team/#jackib) - UX Manager. [Jacki's ReadMe](https://gitlab.com/jackib/jacki-bauer/blob/master/README.md)
* [Tim Noah](https://about.gitlab.com/company/team/#timnoah) - Senior Product Designer, Fulfillment
* [Emily Sybrant](https://about.gitlab.com/company/team/#emilysybrant) - Product Designer, Fulfillment
* [Kevin Comoli](https://about.gitlab.com/company/team/#kcomoli) - Product Designer, Growth
* New Designer (starting September) - Product Designer, Growth
* New Designer (starting October) - Product Designer, Geo and Distribution
* New Designer (starting November) - Product Designer, Ecosystems
* [Evan Read](https://about.gitlab.com/company/team/#eread) - Senior Technical Writer
* [Katherine Okpara](https://about.gitlab.com/company/team/#katokpara) - Junior UX Researcher


##### Some of our key team meetings:
Coming soon: information for new members of our team and for other members of the UX team who would like to see what kinds of touchpoints we have.

## Our strategy
The Growth and Enablement UX teams will be working together to uncover customers core needs, what our users’ workflows looks like, and defining how we can make tasks easier. Becoming strategic involves gathering research, looking for patterns, and making plans for the best path forward for our customers and users. It is also about deciding what we value most, and how to best work together to achieve our goals.

<!-- Copied from Secure's strategy list and made a few changes for our team. I think a lot of what they say applies to us, but am open to all suggestions!-->
We are doing activities like this in order to build our strategy:
Our approach to this work includes:
* [Baseline initiative audit and recommendations](https://about.gitlab.com/handbook/engineering/ux/experience-baseline-recommendations/) (quarterly)
* Internal understanding: stakeholder interviews (annually)
* Customer understand: user research (ongoing)
* Supporting Growth team experiments
* Rapidly prototyping in order to test ideas
* Performing heuristic evaluations on at least 3 competitors, based competitors the 3 user type is using (annually, ongoing)
* We talk to our customer (ongoing)
* We talk to our users (ongoing)
* We outline current user workflows and improve them (upcoming, ongoing)

Additionally, we value the following:
* A Lean UX approach that entails making a hypothesis, using data and experiments to test the hypothesis, implementing winning ideas, and iterating.
* Testing our features with usefulness and usability studies
* Partnering closely with our internal stakeholders in Legal, Support, Finance and Marketing for feedback and feature adoption
* Partnering with our sales and account team to connect directly with customers and learn why customers did (or didn’t) choose our product
* Billing, settings and other functionality that is secondary to the product should work just as well as the product.
* Prioritizing issues that are likely to increase our number of active users

