---
layout: markdown_page
title: "Blueprint"
---

## On this page
{:.no_toc}

- TOC
{:toc}

**Blueprint** content has been moved to the [**Library**](../library/).
