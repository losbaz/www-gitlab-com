---
layout: markdown_page
title: "SO.2.01 - Provisioning Physical Access Control Guidance"
---

## On this page
{:.no_toc}

- TOC
{:toc}

# SO.2.01 - Provisioning Physical Access

## Control Statement

Physical access provisioning to a GitLab datacenter requires management approval and documented specification of:

 * Account type (e.g., standard, visitor, or vendor)
 * Access privileges granted
 * Intended business purpose
 * Visitor identification method, if applicable
 * Temporary badge issued, if applicable
 * Access start date
 * Access duration

## Context

This control refers to physical access to GitLab datacenters.

## Scope

This control is not applicable to GitLab's SaaS product since managed hosting is used for all systems relating to this product. GitLab does not use any datacenters to which any GitLab team-members have any physical access. We rely on the audit certifications for the underlying hosting companies for this control.

## Ownership

TBD

## Implementation Guidance

For detailed implementation guidance relevant to GitLab team-members, refer to the [full guidance documentation](https://gitlab.com/gitlab-com/gl-security/compliance/compliance/blob/master/controls/guidance/SO.2.01_provisioning_physical_access.md).

## Reference Links

For all reference links relevant to this control, refer to the [full guidance documentation](https://gitlab.com/gitlab-com/gl-security/compliance/compliance/blob/master/controls/guidance/SO.2.01_provisioning_physical_access.md).

## Examples of evidence an auditor might request to satisfy this control

For examples of evidence an auditor might request, refer to the [full guidance documentation](https://gitlab.com/gitlab-com/gl-security/compliance/compliance/blob/master/controls/guidance/SO.2.01_provisioning_physical_access.md).

## Framework Mapping

* ISO
  * A.11.1.2
* SOC2 CC
  CC6.4
* PCI
  * 9.2
  * 9.3
  * 9.4
  * 9.4.1
  * 9.4.2
  * 9.5
