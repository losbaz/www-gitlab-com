---
layout: markdown_page
title: "Product Marketing"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Product marketing at GitLab

View the [Product Marketing - Overview Issue Board](https://gitlab.com/gitlab-com/marketing/product-marketing/-/boards/1074672) to see what's currently in progress.

##### Key links
- [Messaging](/handbook/marketing/product-marketing/messaging/)
- [GitLab positioning](/handbook/positioning-faq/)
- [Hidden IT Groups](/handbook/marketing/product-marketing/it-groups/)
- [Defining GitLab roles and personas](/handbook/marketing/product-marketing/roles-personas/)
- [GitLab tiers](/handbook/marketing/product-marketing/tiers/)
- [Market segmentation - Industry verticals](/handbook/marketing/product-marketing/market-segmentation/)
- [Customer presentations](#customer-facing-presentations/)

#### Use Case and Go to Market
[Customer 'use cases'](https://about.gitlab.com/handbook/use-cases/) are a customer problem or initiative that needs a solution and attracts budget, typically defined In customer terms.   In Product Marketing, we need to build content and messaging that engages prospects who are looking for solutions to specific challenges they face.  In product marketing, we will:
1. Research annd prioritize 'use cases'
1. Define the ['buyer's journey'](https://about.gitlab.com/handbook/journeys/#buyer-journey) for a specific usecase
1. Audit our existing content and colloateral for each stage of the buyer's journey
1. Prioritize and refine existing or create new content
1. Collaborate with Content Marketing, Digital Marketing, and SDRs to promote and measure GTM effectiveness.

#### Release vs. launch
A [product release, and a marketing launch are two separate activities](http://www.startuplessonslearned.com/2009/03/dont-launch.html). The canonical example of this is Apple. They launch the iPhone at their yearly event and then release it months later. At GitLab we do it the other way: Release features as soon as they are ready letting customers use them right away, and then, do a marketing launch later when we have market validation.

| Release | Launch |
|-|-|
| PM Led | PMM Led |
| New features can ship with or without marketing support | Launch timing need not be tied to the proximity of when a feature was released |

### Which product marketing manager should I contact?

- Listed below are areas of responsibility within the product marketing team:
  - [William](/company/team/#thewilliamchia), PMM for [CI/CD](/handbook/product/categories/#cicd-section) & [Ops](/handbook/product/categories/#ops-section) Product Sections
  - [Cindy](/company/team/#cblake2000), PMM for [Secure](/handbook/product/categories/#secure-section) & [Defend](/handbook/product/categories/#defend-section) Product Sections
  - [Traci](/company/team/#tracirobinsonwm), Senior Product Marketing Manager, Regulated Industries
  - [Brian](/company/team/##product-marketing-manager,-monitoring), Product Marketing Manager, Monitoring, Create
  - [John](/company/team/#j_jeremiah), Manager PMM for [Dev Product Section](/handbook/product/categories/#dev-section)
  - [Ashish](/company/team/#kuthiala), Director PMM
